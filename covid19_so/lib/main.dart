import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:covid19so/pages/newsListPage.dart';
import 'package:covid19so/viewmodels/newsArticleListViewModel.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Covid-19 About'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text("Covid-19 About"),
              centerTitle: true,
              backgroundColor: Colors.cyan[900],
            ),
            body: Stack(children: <Widget>[
              Positioned(
                top: 0.0,
                left: -10,
                child: Image.asset(
                  "images/covid19.jpg",
                  height: 230.0,
                  width: 425.0,
                  fit: BoxFit.contain,
                ),
              ),
              Positioned(
                  top: 341.0,
                  left: -20.0,
                  child: FlatButton(
                    child: Image.asset(
                      "images/interrogation.jpg",
                      height: 115.0,
                      width: 425.0,
                      fit: BoxFit.fill,
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => ScreenInfo()));
                    },
                  )),
              Positioned(
                top: 564.0,
                left: -10.0,
                child: Image.asset(
                  "images/covid2.jpg",
                  height: 40.0,
                  width: 425.0,
                  fit: BoxFit.fitWidth,
                ),
              ),
              Positioned(
                top: 449.0,
                left: -20.0,
                child: FlatButton(
                  child: Image.asset(
                    "images/covid_noticia.jpg",
                    height: 115.0,
                    width: 425.0,
                    fit: BoxFit.fitWidth,
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        CupertinoPageRoute(builder: (context) => Screen3()));
                  },
                ),
              ),
              Positioned(
                top: 226.0,
                left: -20.0,
                child: FlatButton(
                  child: Image.asset(
                    "images/calculadora.jpg",
                    height: 115.0,
                    width: 425.0,
                    fit: BoxFit.fitWidth,
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        CupertinoPageRoute(builder: (context) => Screen2()));
                  },
                ),
              ),
            ])));
  }
}

class Screen2 extends StatefulWidget {
  @override
  _Screen2State createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  TextEditingController weekController = TextEditingController();
  TextEditingController caseController = TextEditingController();

  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  String _infoText = "Informe os dados";

  void _resetFields() {
    weekController.text = "";
    caseController.text = "";
    setState(() {
      _infoText = "Informe os dados!";
    });
  }

  void _calculate() {
    setState(() {
      //double funcao = 2;
      double dias = 7;
      //double tempo = 2;
      //double tempo2 = 1;
      double week = double.parse(weekController.text);
      double cases = double.parse(caseController.text);
      double death = ((week * dias) * ((cases * 2) - 1));
      print(death);
      _infoText = "Número de casos confirmados (${death})";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Calculadora Covid-19"),
          centerTitle: true,
          backgroundColor: Colors.cyan[900],
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: _resetFields,
            ),
          ],
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(10.0, 50.0, 10.0, 0.0),
          child: Form(
            key: _formkey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Icon(Icons.trending_up, size: 150.0, color: Colors.redAccent),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "Casos:",
                      labelStyle: TextStyle(color: Colors.red[900])),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 25.0),
                  controller: caseController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Insira o número de casos!";
                    }
                  },
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "Número de semanas:",
                      labelStyle: TextStyle(color: Colors.red[900])),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 25.0),
                  controller: weekController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Insira o número de semanas!";
                    }
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Container(
                      height: 50.0,
                      child: RaisedButton(
                        onPressed: () {
                          if (_formkey.currentState.validate()) {
                            _calculate();
                          }
                        },
                        child: Text("Calcular",
                            style:
                                TextStyle(color: Colors.white, fontSize: 25.0)),
                        color: Colors.cyan[900],
                      )),
                ),
                Text(
                  _infoText,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.red[900], fontSize: 25.0),
                )
              ],
            ),
          ),
        ));
  }
}

class ScreenInfo extends StatefulWidget {
  @override
  _ScreenInfoState createState() => _ScreenInfoState();
}

class _ScreenInfoState extends State<ScreenInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Informações"),
        centerTitle: true,
        backgroundColor: Colors.cyan[900],
      ),
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 175.0,
            left: -10,
            child: Image.asset(
              "images/coronabackground.jpg",
              height: 230.0,
              width: 425.0,
              fit: BoxFit.contain,
            ),
          ),
          Positioned(
            top: 50.0,
            left: 60.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("O que deseja acessar?",
                        style: TextStyle(
                          color: Colors.cyan[600],
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic,
                          fontSize: 30.0,
                        )),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            top: 425.0,
            left: 20.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: FloatingActionButton.extended(
                        heroTag: "btn1",
                        backgroundColor: Colors.cyan[900],
                        icon: Icon(Icons.account_balance),
                        label: Text("Prevenção"),
                        onPressed: () {
                          Navigator.push(
                              context,
                              CupertinoPageRoute(
                                  builder: (context) => ScreenInfoprev()));
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: FloatingActionButton.extended(
                        heroTag: "btn2",
                        backgroundColor: Colors.cyan[900],
                        icon: Icon(Icons.accessibility),
                        label: Text("Sintomas"),
                        onPressed: () {
                          Navigator.push(
                              context,
                              CupertinoPageRoute(
                                  builder: (context) => ScreenSint()));
                        },
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: FloatingActionButton.extended(
                        heroTag: "btn3",
                        backgroundColor: Colors.cyan[900],
                        icon: Icon(Icons.call),
                        label: Text("Quem Contatar"),
                        onPressed: () {
                          Navigator.push(
                              context,
                              CupertinoPageRoute(
                                  builder: (context) => ScreenContato()));
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: FloatingActionButton.extended(
                        heroTag: "btn4",
                        backgroundColor: Colors.cyan[900],
                        icon: Icon(Icons.android),
                        label: Text("Sobre o App"),
                        onPressed: () {
                          Navigator.push(
                              context,
                              CupertinoPageRoute(
                                  builder: (context) => ScreenInfoApp()));
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ScreenInfoprev extends StatefulWidget {
  @override
  _ScreenInfoprevState createState() => _ScreenInfoprevState();
}

class _ScreenInfoprevState extends State<ScreenInfoprev> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Prevenção"),
          centerTitle: true,
          backgroundColor: Colors.cyan[900],
        ),
        body: Stack(
          children: <Widget>[
            Positioned(
              top: 100.0,
              left: -10,
              child: Image.asset(
                "images/prev6.jpg",
                height: 400.0,
                width: 425.0,
                fit: BoxFit.fitWidth,
              ),
            ),
          ],
        ));
  }
}

class ScreenSint extends StatefulWidget {
  @override
  _ScreenSintState createState() => _ScreenSintState();
}

class _ScreenSintState extends State<ScreenSint> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Sintomas"),
          centerTitle: true,
          backgroundColor: Colors.cyan[900],
        ),
        body: Stack(
          children: <Widget>[
            Positioned(
              top: 100.0,
              left: -10,
              child: Image.asset(
                "images/sintomas3.png",
                height: 400.0,
                width: 425.0,
                fit: BoxFit.contain,
              ),
            ),
          ],
        ));
  }
}

class ScreenContato extends StatefulWidget {
  @override
  _ScreenContatoState createState() => _ScreenContatoState();
}

class _ScreenContatoState extends State<ScreenContato> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Quem Contatar"),
          centerTitle: true,
          backgroundColor: Colors.cyan[900],
        ),
        body: Stack(
          children: <Widget>[
            Positioned(
              top: 100.0,
              left: -10,
              child: Image.asset(
                "images/contato.jpeg",
                height: 400.0,
                width: 425.0,
                fit: BoxFit.fill,
              ),
            ),
          ],
        ));
  }
}

class ScreenInfoApp extends StatefulWidget {
  @override
  _ScreenInfoAppState createState() => _ScreenInfoAppState();
}

class _ScreenInfoAppState extends State<ScreenInfoApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Sobre o App"),
          centerTitle: true,
          backgroundColor: Colors.cyan[900],
        ),
        body: Stack(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                    "Aplicativo desenvolvido com o objetivo de levar informação de um jeito simples e intuitivo,"
                        "alertando a todos a importância de seguir as recomendações dos especialistas da saúde sobre o COVID-19.",
                style: TextStyle(
                  color: Colors.cyan[900],
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                  fontSize: 20.0
                ),
                textAlign: TextAlign.center,
                ),
                Text(
                    "Projeto desenvolvido por: Enzo Hanai Catsuqui, Thierry Vasconcelos Oliveira Araújo, Matheus Ricardo Soares de Lima.",
                  style: TextStyle(
                      color: Colors.cyan[900],
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 20.0
                  ),
                  textAlign: TextAlign.center,
                ),
                Text(
                  "Orientador do projeto: Aderbal Botelho Leite Neto.",
                  style: TextStyle(
                      color: Colors.cyan[900],
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 20.0
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ],
        ));
  }
}

class Screen3 extends StatefulWidget {
  @override
  _Screen3State createState() => _Screen3State();
}

class _Screen3State extends State<Screen3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Covid-19 News"),
        centerTitle: true,
        backgroundColor: Colors.cyan[900],
      ),
      body: ChangeNotifierProvider(
          builder: (_) => NewsArticleListViewModel(), child: NewsListPage()),
    );
  }
}
