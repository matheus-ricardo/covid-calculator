import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Covid-19 About'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Widget build(BuildContext context) {
    return MaterialApp (
        home: Scaffold(
            appBar: AppBar(
              title: Text("Covid-19 About."),
              centerTitle: true,
            ),
            body: Stack(
                children: <Widget>[
                  Image.asset(
                    "images/coronabackground.jpg",
                    height: 200.0,
                    width: double.infinity,
                    fit: BoxFit.fill,
                  ),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget> [
                        Text("Bem Vindo ao app Covid-19.",
                            style: TextStyle(
                                color: Colors.blueAccent,
                                fontWeight: FontWeight.bold,
                                fontSize: 30.0)),
                        Text("Qual opção deseja acessar?",
                            style: TextStyle(
                              color: Colors.blueAccent,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              fontSize: 30.0,
                            )),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(10.0),
                              child: FlatButton(
                                child: Text("Calculadora",
                                    style: TextStyle(
                                        fontSize: 30.0, color: Colors.blueAccent)),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                          builder: (context) => Screen2()));
                                },
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(10.0),
                              child: FlatButton(
                                child: Text("Info",
                                    style: TextStyle(
                                        fontSize: 30.0, color: Colors.blueAccent)),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                          builder: (context) => Screen3()));
                                },
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(10.0),
                              child: FlatButton(
                                child: Text("Mapa",
                                    style: TextStyle(
                                        fontSize: 30.0, color: Colors.blueAccent)),
                                onPressed: () {},
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(10.0),
                              child: FlatButton(
                                child: Text("Não sei",
                                    style: TextStyle(
                                        fontSize: 30.0, color: Colors.blueAccent)),
                                onPressed: () {},
                              ),
                            ),
                          ],
                        ),
                      ],
                  ),
                ],
            ),
        ),
    );
  }
}

class Screen2 extends StatefulWidget {
  @override
  _Screen2State createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  TextEditingController weekController = TextEditingController();
  TextEditingController caseController = TextEditingController();

  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  String _infoText = "Informe os dados";

  void _resetFields() {
    weekController.text = "";
    caseController.text = "";
    setState(() {
      _infoText = "Informe os dados!";
    });
  }

  void _calculate() {
    setState(() {
      //double funcao = 2;
      double dias = 7;
      //double tempo = 2;
      //double tempo2 = 1;
      double week = double.parse(weekController.text);
      double cases = double.parse(caseController.text);
      double death = ((week * dias) * ((cases * 2) - 1));
      print(death);
      _infoText = "Número de casos confirmados (${death})";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Calculadora Covid-19"),
          centerTitle: true,
          backgroundColor: Colors.blueAccent,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.apps),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: _resetFields,
            ),
          ],
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(10.0, 50.0, 10.0, 0.0),
          child: Form(
            key: _formkey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Icon(Icons.trending_up, size: 150.0, color: Colors.redAccent),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "Casos:",
                      labelStyle: TextStyle(color: Colors.blueAccent)),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.blueAccent, fontSize: 25.0),
                  controller: caseController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Insira o número de casos!";
                    }
                  },
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "Número de semanas:",
                      labelStyle: TextStyle(color: Colors.blueAccent)),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.blueAccent, fontSize: 25.0),
                  controller: weekController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Insira o número de semanas!";
                    }
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Container(
                      height: 50.0,
                      child: RaisedButton(
                        onPressed: () {
                          if (_formkey.currentState.validate()) {
                            _calculate();
                          }
                        },
                        child: Text("Calcular",
                            style:
                            TextStyle(color: Colors.white, fontSize: 25.0)),
                        color: Colors.blueAccent,
                      )),
                ),
                Text(
                  _infoText,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.blueAccent, fontSize: 25.0),
                )
              ],
            ),
          ),
        ));
  }
}

class Screen3 extends StatefulWidget {
  @override
  _Screen3State createState() => _Screen3State();
}

class _Screen3State extends State<Screen3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Screen 3"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

Navigator.push(
context,
CupertinoPageRoute(
builder: (context) => Screen2()));


